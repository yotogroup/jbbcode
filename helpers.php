<?php

/**
 * Transform bbcode to html
 *
 * @param string $str
 * @return string
 */
function bbcode_to_html($str)
{
    $parser = new \JBBCode\Parser();
    $parser->addCodeDefinitionSet(new \JBBCode\DefaultCodeDefinitionSet());

    $parser->parse($str);

    return $parser->getAsHTML();
}